# Provider name
provider "aws" {
  region = var.region
}

# Storing state file on S3 backend
terraform {
  backend "s3" {
    bucket = "dibo-directive-tf-state/jenkins-tf/"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}